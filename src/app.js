(function () {
    var formText = document.querySelector(".form__text");
    var formColor = document.querySelector(".form__color");
    var formBtnAdd = document.querySelector(".form__btn--addjs");
    var formBtnClear = document.querySelector(".form__btn--clearjs")
    var listContainer = document.querySelector(".list__container");
    var emptyList = document.querySelector(".empty-list");


    function enableBtn(btnNode) {
        btnNode.classList.add("form__btn--enabled");
        btnNode.classList.remove("form__btn--disabled");
        btnNode.removeAttribute("disabled");
    }

    function disableBtn(btnNode) {
        btnNode.setAttribute("disabled", "disabled");
        btnNode.classList.remove("form__btn--enabled");
        btnNode.classList.add("form__btn--disabled");
    }

    function showHideLabel() {
        var childList = listContainer.childNodes.length;
        var list = document.querySelector(".list");

        if (childList === 0) {
            emptyList.classList.remove("empty-list--hidden");
            list.classList.add("list--hidden");
            disableBtn(formBtnClear);
        } else if (childList > 0) {
            emptyList.classList.add("empty-list--hidden");
            list.classList.remove("list--hidden");
            enableBtn(formBtnClear);
        }
    }

    function onChangeHandler() {
        var text = formText.value;
        var length = text.trim().length;
        if (length > 0) {
            enableBtn(formBtnAdd);
        } else {
            disableBtn(formBtnAdd);
        }
    }

    function hotKeyInter(e) {
        if (e.keyCode === 13) {
            e.preventDefault();
            validateForm();
        }
    }

    function deleteAllItems() {
        if (confirm("Вы действительно хотите удалить весь список?")) {
            while (listContainer.childNodes[0]) {
                listContainer
                    .childNodes[0]
                    .querySelector(".list__btn")
                    .removeEventListener('click', deleteItem);
                listContainer.removeChild(listContainer.childNodes[0]);
            }
        }
        showHideLabel();
    }

    function deleteItem(event) {
        if (confirm("Вы действительно хотите удалить элемент списка?")) {
            event.srcElement.removeEventListener('click', deleteItem);
            listContainer.removeChild(event.srcElement.parentNode);
        }
        showHideLabel();
    }

    function createItem(text, color) {
        var elementLi = document.createElement("li");
        elementLi.style.color = color;
        elementLi.className = "list__item";

        var listBtn = document.createElement("button");
        listBtn.className = "list__btn";

        elementLi.innerHTML = text;
        elementLi.appendChild(listBtn);

        listContainer.appendChild(elementLi);

        listBtn.addEventListener("click", deleteItem);
        showHideLabel();
    }

    function validateForm() {
        var text = formText.value;
        var color = formColor.value;
        var length = text.trim().length;
        if (length === 0) {
            formBtnAdd.setAttribute("disabled");
        }
        else {
            createItem(text, color);
            formText.value = "";
            onChangeHandler();
        };
    }

    formBtnClear.addEventListener("click", deleteAllItems);
    formBtnAdd.addEventListener("click", validateForm);
    formText.addEventListener("input", onChangeHandler);
    formText.addEventListener("keydown", hotKeyInter, false);


})();
