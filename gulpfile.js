var gulp = require('gulp');
var autoprefixer = require('gulp-autoprefixer');
 
gulp.task('autoprefixer', function () {
    return gulp.src('src/global.css')
        .pipe(autoprefixer({
            browsers: ['last 2 versions'],
            cascade: false
        }))
        .pipe(gulp.dest('src/autoprefixer/'));
});